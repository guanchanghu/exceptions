<?php

namespace GuanChanghu\Exceptions;

/**
 * @author 管昌虎
 * Class AdminException
 * @package GuanChanghu\Exceptions
 * Created on 2022/7/4 16:25
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class AdminException extends LogException
{
    /**
     * 错误码
     */
    public const CODE = 412;

    /**
     * 错误码
     */
    protected $code = self::CODE;
}
