<?php

namespace GuanChanghu\Exceptions;

/**
 * @author 管昌虎
 * Class NeedLoginException
 * @package GuanChanghu\Exceptions
 * Created on 2022/6/16 15:46
 * Created by 管昌虎
 * Email guanchanghu626@163.com
 */
class NeedLoginException extends LogException
{
    /**
     * 错误码
     */
    public const CODE = 401;

    /**
     * 错误码
     */
    protected $code = self::CODE;
}
