<?php

namespace GuanChanghu\Exceptions;

/**
 * Class DeveloperException
 * @package GuanChanghu\Exceptions
 */
class DeveloperException extends LogException
{
    /**
     * 错误码
     */
    public const CODE = 500;

    /**
     * 错误码
     */
    protected $code = self::CODE;
}
