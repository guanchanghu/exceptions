<?php

namespace GuanChanghu\Exceptions;

/**
 * Class UserException
 * @package GuanChanghu\Exceptions
 */
class UserException extends LogException
{
    /**
     * 错误码
     */
    public const CODE = 406;

    /**
     * 错误码
     */
    protected $code = self::CODE;
}
